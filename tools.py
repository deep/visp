import functools
import itertools
import pathlib
import typing

import numpy as np
import pyproj
import xarray as xr
from scipy.interpolate import CloughTocher2DInterpolator
from scipy.ndimage import distance_transform_edt

import salvus.utils.xarray_tools as xr_tools


def _parse_lines(lines: typing.List[str]):
    return np.fromstring(
        "".join(lines).replace("\n", "").replace("-", " -").strip(), sep=" "
    )


def read_velomod(filename: str) -> xr.DataArray:
    """
    Read a velomod file and return an xarray DataArray.

    Args:
        filename: The filename to read.

    Returns:
        A DataArray.
    """

    with open(filename, mode="r") as fh:
        lines = fh.readlines()

    x = _parse_lines(lines[1:4]) * -1000.0
    y = _parse_lines(lines[4:7]) * 1000.0
    z = _parse_lines(lines[7:8]) * -1000.0
    v = _parse_lines(lines[9:]) * 1000.0

    nx, ny, nz = len(x), len(y), len(z)

    # Reference locations HERE IN ED50 N32, epsg23032: 462069.582191081,
    # 5205484.598682921
    rx, ry = (462069.582191081, 5205484.598682921)

    # reference location in epsg32632:
    # (461987.2450072905, 5205285.41538475)

    return xr.DataArray(
        np.reshape(v, (nz, ny, nx), order="C"),
        [("z", z), ("y", y + ry), ("x", x + rx)],
    ).transpose("x", "y", "z")


def read_visp(
    visp_model_dir: typing.Union[str, pathlib.Path],
    l_idx: int,
    decimate: int = 1,
) -> xr.DataArray:
    """
    Read the visp model.

    The visp model is parametrized in terms of depth below the surface.
    This function only reads the depths; the actual velocities to assign to
    each layer are defined elsewhere.

    Args:
        l_idx: The layer index.
        decimate: Decimate the model by this factor. Useful to increase
            this on systems with limited memory.

    Returns:
        The visp model depths as a 2-D surface.
    """

    # Read the text file containing the model. Columns are x, y in the
    # CH1903/LV03 reference coordinate system, and then depth in meters
    # below the surface.
    x, y, d = np.loadtxt(
        pathlib.Path(visp_model_dir) / f"L{l_idx}_adjusted_small_6th.xyz"
    ).T

    # Get the unique coordinates. We will use these to build a grid.
    xu, yu = map(np.unique, [x, y])

    # Build the grid and return as a `DataArray`. Note that the y values in
    # the model are ordered in decrease order, and as such are reversed
    # here.
    return xr_tools.sort_by_dim(
        xr.DataArray(
            np.reshape(d, (len(yu), len(xu)))[::decimate, ::decimate],
            [("y", yu[::decimate][::-1]), ("x", xu[::decimate])],
        )
    )


def generate_blended_model(
    visp_model_dir: typing.Union[pathlib.Path, str], topo_dem: xr.DataArray
):
    """
    Blend the Visp model into the Roten model.

    Reads the Visp depths and transforms the horizontal coordinates into the
    mesh's UTM zone. Generates a series of model grids and applies the Visp and
    Roten models to these grids. Then, blends the Visp model into the Roten
    model by way of a taper based on a Euclidean Distance transform.

    Args:
        visp_model_dir: The directory storing the visp model.
        topo_dem: The digital elevation model for the surface.
    """

    def _transform_coordinates(visp_layer: xr.DataArray) -> xr.DataArray:
        """
        Transform the visp model's coordinates into the mesh's UTM zone.

        Args:
            visp_layer: The DataArray containing the depths of the visp model
                for a layer.

        Returns:
            The visp model depths transformed into the mesh's UTM zone and
            re-interpolated onto a regular grid.
        """

        x, y = xr_tools.flatten_coordinates(visp_layer.T).T

        # Transform to EPSG:32632 from CH1903/LV03 (EPSG:21781).
        _x, _y = pyproj.Transformer.from_crs(
            "EPSG:21781", "EPSG:32632"
        ).transform(x, y)

        # Re-interpolate onto a regular grid that is identical to the grid of
        # the surface topography.
        i = CloughTocher2DInterpolator(
            np.c_[_x, _y], visp_layer.values.ravel(), fill_value=0.0
        )(xr_tools.flatten_coordinates(topo_dem))

        # Wrap the transformed depths in a new `DataArray`.
        da = xr.DataArray(
            i.reshape(topo_dem.shape),
            [("x", topo_dem.x.values), ("y", topo_dem.y.values)],
        )

        # Post process interpolated array to remove spline overshoots.
        return xr.where(np.abs(_da := da.clip(None, 0.0)) < 0.1, 0.0, _da)

    def _reduce_roten(
        da: xr.DataArray,
        i_face_and_par: typing.Tuple[typing.Tuple[float, float], float],
    ):
        """
        Given a model grid `da` add the Roten model layering.

        Args:
            da: The allocated model grid. Should span the domain of interest.
            i_face_and_par: A tuple containing two entries:

                1. A tuple of two floats containing the depths of the top and
                   bottom of a single Roten layer, respectively.
                2. The parameters to assign within the layer.

        Returns:
            A new `DataArray` where the locations within a layer are set to the
            desired parameter, and the locations outside the layer are left
            unchanged.
        """

        # Broadcast the coordinates over the model grid. Similar to `meshgrid`.
        crds = xr_tools.broadcast_coords(da)

        # Unpack the input tuples to the (top, bot) and the parameter value.
        (i_top, i_bot), par = i_face_and_par

        # Within layer set the model to `par`, outside retain current value.
        return xr.where((crds.depth < i_bot) & (crds.depth >= i_top), par, da)

    def _add_visp(
        da: xr.DataArray,
        d_top: xr.DataArray,
        d_bot: xr.DataArray,
        b_dist: float = 400.0,
        **pars: float,
    ):
        """
        Given a model grid `da`, add a layer of the Visp model.

        Args:
            da: The model grid for a given parameter.
            d_top: A `DataArray` defining the depths of the top of a layer.
            d_bot: A `DataArray` defining the depths of the bottom of a layer.
            b_dist: The distance in meters over which to blend the visp model
                into the background.
            pars: A dictionary mapping a parameter name to its value within the
                layer defined by `d_top` and `d_bot`.

        Returns:
            The model contained within `da` with the Visp model blended in.
        """

        # Broadcast the coordinates over the model grid. Similar to `meshgrid`.
        crds = xr_tools.broadcast_coords(da)

        # Get the spatial sampling of the model in the x and y dimensions.
        dx = [np.diff(d_bot.x)[0], np.diff(d_bot.y)[0]]

        # Create a taper based on a Euclidean Distance Transform, and clip this
        # taper between (0.0, b_dist). This latter stager ensures that, when we
        # are farther than b_dist from the boundary of the visp model, we have
        # no contributions from the background model. Between 0 and b_dist
        # meters from the boundary we have a linear taper into the background
        # model. For more info on the Euclidean Distance Transform (EDT) see
        # https://docs.scipy.org/doc/scipy/reference/generated/
        # scipy.ndimage.distance_transform_edt.html.
        mask = distance_transform_edt(d_bot < 0, dx).clip(None, b_dist)

        # Normalize the EDT between 0 and 1 to make it suitable for blending.
        mask /= np.ptp(mask)

        return xr.where(
            # Are we within or outside a layer of the Visp model?
            (crds.depth >= np.abs(d_top)) & (crds.depth < np.abs(d_bot)),
            # If inside, add the Visp model tapered into the background ...
            da * (1 - mask) + pars[str(da.name)] * mask,
            # ... if we are outside, just use the background.
            da,
        )

    # Read the visp model depths.
    print("Reading visp model...")
    l1_d, l2_d, l3_d = (read_visp(visp_model_dir, i) for i in range(1, 4))
    l1_d, l2_d, l3_d = map(_transform_coordinates, [l1_d, l2_d, l3_d])

    # Generate an empty 3-D model spanning the visp / Roten depths.
    skeleton = xr.DataArray(
        coords=[
            ("x", l1_d.x.values),
            ("y", l1_d.y.values),
            ("depth", np.r_[np.arange(56) * 10.0, 560.0]),
        ]
    )

    # Parameters of the Roten model.
    roten_dsc = [0.0, 75.0, 225.0, 400.0, 500.0, 560.0]
    vp_roten_vals = [1600.0, 1900.0, 1900.0, 1900.0, 2000.0]
    vs_roten_vals = [320.0, 480.0, 750.0, 800.0, 900.0]
    rho_roten_vals = [1600.0, 1900.0, 1900.0, 1900.0, 2000.0]

    # Fill in the model with the Roten parameters.
    print("Blending visp model...")
    ds = xr.Dataset(
        {
            "VP": functools.reduce(
                _reduce_roten,
                zip(itertools.pairwise(roten_dsc), vp_roten_vals),
                skeleton,
            ),
            "VS": functools.reduce(
                _reduce_roten,
                zip(itertools.pairwise(roten_dsc), vs_roten_vals),
                skeleton,
            ),
            "RHO": functools.reduce(
                _reduce_roten,
                zip(itertools.pairwise(roten_dsc), rho_roten_vals),
                skeleton,
            ),
        }
    )

    # Parameters of the Visp model.
    print("Computing Visp model...")
    p_visp_l1 = {"VP": 914.0, "VS": 210.0, "RHO": 1795.0}
    p_visp_l2 = {"VP": 1571.0, "VS": 381.0, "RHO": 1900.0}

    # L3 is trickier. Email from Maria. Please check -- Vs values become faster
    # than the Roten model it is embedded within. This may be desirable, but
    # they may also need to be clipped.
    z_minus_z0 = ds.depth - np.abs(l2_d)
    visp_l3_vs = 1494 - 1035 * np.exp(-0.005 * z_minus_z0)
    visp_l3_vp = 2.38 + 1.12 * np.exp(-0.009 * (z_minus_z0)) * visp_l3_vs
    p_visp_l3 = {"VP": visp_l3_vp, "VS": visp_l3_vs, "RHO": 2100.0}

    # Blend each layer of the Visp model into the Roten model.
    print("Blending Visp model...")
    ds = ds.map(_add_visp, d_top=xr.DataArray([0.0]), d_bot=l1_d, **p_visp_l1)
    ds = ds.squeeze().map(_add_visp, d_top=l1_d, d_bot=l2_d, **p_visp_l2)
    ds = ds.squeeze().map(_add_visp, d_top=l2_d, d_bot=l3_d, **p_visp_l3)
    ds = ds.ffill(dim="depth")

    return ds.assign_coords(
        depth=1 - ds.depth / np.ptp(ds.depth.values)
    ).rename(depth="v_relative")
