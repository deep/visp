Local model of the Rhone Valley for Maria Koroni and the SED. Consists of three
3-D models, topography, and several layers.

Requires a `data` directory with the models. The data dir should look like:

```
❯ tree data/
data/
├── Rhone
│   └── Visp
│       └── 8_3DModel_xyz
│           ├── Visp_Small_6th
│           │   ├── L1_adjusted_small_6th.xyz
│           │   ├── L2_adjusted_small_6th.xyz
│           │   └── L3_adjusted_small_6th.xyz
├── velomod_P.out
└── velomod_S.out
```
