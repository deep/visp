# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.16.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# Import Salvus and other tools
import os
import pathlib

import numpy as np
import xarray as xr
import salvus.namespace as sn
import salvus.mesh.layered_meshing as lm
import salvus.utils.xarray_tools as xr_tools
import tools
from scipy.interpolate import CloughTocher2DInterpolator
import pyproj
from scipy.ndimage import distance_transform_edt

# -

# # Create domain

# +
# Small domain.
min_lat, max_lat = 46.14, 46.44
min_lon, max_lon = 7.73, 8.03

# Create domain.
d = sn.domain.dim3.UtmDomain.from_spherical_chunk(
    min_latitude=min_lat,
    max_latitude=max_lat,
    min_longitude=min_lon,
    max_longitude=max_lon,
)

# Visualize.
d.plot()
# -

# # Get topography

# +
# Download topography.
topo_filename = "./gmrt_topography_Rhone.nc"

# Query data from the GMRT web service.
if not os.path.exists(topo_filename):
    d.download_topography_and_bathymetry(
        filename=topo_filename,
        # The buffer is useful for example when adding sponge layers
        # for absorbing boundaries so we recommend to always use it.
        buffer_in_degrees=0.01,
        resolution="high",
    )

# Resample and use in Salvus.
s_topo_gmrt = sn.topography.cartesian.SurfaceTopography.from_gmrt_file(
    name="ch",
    data=topo_filename,
    utm=d.utm,  # if crs_utm is not defined above, where does it find the utm?
    decimate_topo_factor=10,  # 10, try 100, 200 to make snippet faster
    # resample_topo_nx=1501,
    resample_topo_nx=501,
    gaussian_std_in_meters=100.0,  # 1000
).ds.dem.T

# Adjust reference coordinate.
s_topo_gmrt -= s_topo_gmrt.max()

# Make sure it is sorted correctly!
s_topo_gmrt = xr_tools.sort_by_dim(s_topo_gmrt)

# Plot.
s_topo_gmrt.plot()
# -

# # Create velocity model
#
# These are the layers from Roten et al., 2008. Use interfaces which follow the surface topography.
#
#
# | z (m)   |  Vp (m/s)  |  Vs (m/s)  |  ρ(kg/m^3) |  Qp   |   Qs   |   Geological interpretation |
# | ------- |  --------- |  --------- |  --------- | ----- | ------ |   --------- |
# | 0–225   |  1700      |  700       |  1700      |  25   |   25   |   Alluvial fans |
# | 0–75    |  1600      |  320       |  1600      |  25   |   25   |   Deltaic sediments |
# | 75–225  |  1900      |  480       |  1900      |  25   |   25   |   Lacustrine deposits |
# | 225–400 |  1900      |  750       |  1900      |  25   |   25   |   Glaciolacustrine deposits |
# | 400–500 |  1900      |  800       |  1900      |  25   |   25   |   Meltout and reworked till |
# | 500–560 |  2000      |  900       |  2000      |  25   |   25   |   Subglacial deposits |
# | 0–750   |  4000      |  2325      |  2500      |  100  |   100  |   Bedrock |

# ## Generate the blended model -- standalone function.

mod = tools.generate_blended_model(
    "./data/Rhone/Visp/8_3DModel_xyz/Visp_Small_6th", s_topo_gmrt
)

# ## Plot the blended model.

mod.VS.sel(v_relative=1.0, method="nearest").plot()

# ## Add bedrock

# +
# Parameters.
vb_vs = 1560.0
vb_vp = 2560.0
vb_rho = 2600.0

# 400 meters above the valley floor: switch to bedrock.
elevation_for_bedrock = 400 + s_topo_gmrt.min()

# Modify the model.
mod["VS"] = xr.where(s_topo_gmrt > elevation_for_bedrock, vb_vs, mod.VS)
mod["VP"] = xr.where(s_topo_gmrt > elevation_for_bedrock, vb_vp, mod.VP)
mod["RHO"] = xr.where(s_topo_gmrt > elevation_for_bedrock, vb_rho, mod.RHO)
# -

# ## Plot model with bedrock

mod.VP.sel(v_relative=1.0, method="nearest").plot()

# ## Set up layers

# +
surface = lm.interface.Surface(s_topo_gmrt)

bottom_of_roten = lm.interface.Surface(
    s_topo_gmrt.assign_attrs(reference_elevation=lm.interface.Depth(560.0))
)

vb = lm.material.elastic.Velocity.from_params(rho=vb_rho, vp=vb_vp, vs=vb_vs)

bottom_of_vb = lm.interface.Hyperplane.at(-500.0)

bedrock = lm.material.elastic.Velocity.from_params(
    rho=2000,
    vp=tools.read_velomod("data/velomod_P.out"),
    vs=tools.read_velomod("data/velomod_S.out"),
)

bottom_of_mesh = lm.interface.Hyperplane.at(-15000.0)
# -

# ## Bring together in a layered model

layered_model = lm.LayeredModel(
    [
        surface,
        lm.material.elastic.Velocity.from_dataset(
            mod.isel(x=slice(None, None, 10), y=slice(None, None, 10))
        ),
        bottom_of_roten,
        vb,
        bottom_of_vb,
        bedrock,
        bottom_of_mesh,
    ]
)

# ## Generate the mesh

mesh = lm.mesh_from_domain(
    domain=d,
    model=lm.MeshingProtocol(
        layered_model,
        intralayer_coarsening_policy=[
            lm.IntralayerVerticalRefine(refinement_type="doubling"),
            lm.IntralayerVerticalRefine(refinement_type="tripling"),
            lm.IntralayerConstant(),
        ],
        interlayer_coarsening_policy=[
            lm.InterlayerConstant(),
            lm.InterlayerDoubling(),
        ],
    ),
    mesh_resolution=sn.MeshResolution(reference_frequency=1.0),
)

mesh

# ## Next steps
#
# Can add this mesh to Salvus Project using an [UnstructuredMeshSimulationConfiguration] or use it to run a simulation using SalvusFlow.
